package ru.baulina.tm.repository;

import ru.baulina.tm.entity.Task;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.api.repository.IUserRepository;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    private List<User> users = new ArrayList<>();

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public User add(final User user) {
        users.add(user);
        return user;
    }

    @Override
    public User findById(final Long id) {
        for (final User user: users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User removeUser(User user) {
        users.remove(user);
        return user;
    }

    @Override
    public User findByLogin(final String login) {
        for (final User user: users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User removeById(final Long id) {
        final User user = findById(id);
        if (user == null) return null;
        return removeUser(user);
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        return removeUser(user);
    }

    @Override
    public void merge(List<User> users) {
        if (users == null) return;
        for (final User user: users) merge(user);
    }

    @Override
    public void merge(User... users) {
        if (users == null) return;
        for (final User user: users) merge(user);
    }

    @Override
    public User merge(User user) {
        if (user == null) return null;
        users.add(user);
        return user;
    }

    @Override
    public void load(List<User> users) {
        clear();
        merge(users);
    }

    @Override
    public void load(User... users) {
        clear();
        merge(users);
    }

    @Override
    public void clear() {
        users.clear();
    }

    @Override
    public List<User> getListUser() {
        final List<User> result = new ArrayList<>();
        for (final User user: users) {result.add(user);}
        return result;
    }

}
