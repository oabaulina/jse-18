package ru.baulina.tm.repository;

import ru.baulina.tm.api.repository.ITaskRepository;
import ru.baulina.tm.entity.Project;
import ru.baulina.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final Long userId, final Task task) {
        task.setUserId(userId);
        //tasks.add(task);
        merge(task);
    }

    @Override
    public void remove(final Long userId, final Task task) {
        if (userId.equals(task.getUserId())) tasks.remove(task);
    }

    @Override
    public List<Task> findAll(final Long userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task: tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public List<Task> getListTask() {
        final List<Task> result = new ArrayList<>();
        for (final Task task: tasks) {result.add(task);}
        return result;
    }

    @Override
    public void clear(final Long userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task: tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        result.clear();
    }

    @Override
    public Task findOneById(final Long userId, final Long id) {
        for (final Task task: tasks) {
            if (userId.equals(task.getUserId()) && id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findOneByIndex(final Long userId, final Integer index) {
        final List<Task> result = new ArrayList<>();
        for (final Task task: tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result.get(index);
    }

    @Override
    public Task findOneByName(final Long userId, final String name) {
        for (final Task task: tasks) {
            if (userId.equals(task.getUserId()) && name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneById(final Long userId, final Long id) {
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeOneByIndex(final Long userId, final Integer index) {
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null ;
        remove(userId, task);
        return task;
    }

    @Override
    public Task removeOneByName(final Long userId, final String name) {
        final Task task = findOneByName(userId, name);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Override
    public void merge(List<Task> tasks) {
        if (tasks == null) return;
        for (final Task task: tasks) merge(task);
    }

    @Override
    public void merge(Task... tasks) {
        if (tasks == null) return;
        for (final Task task: tasks) merge(task);
    }

    @Override
    public Task merge(Task task) {
        if (task == null) return null;
        tasks.add(task);
        return task;
    }

    @Override
    public void load(List<Task> tasks) {
        clear();
        merge(tasks);
    }

    @Override
    public void load(Task... tasks) {
        clear();
        merge(tasks);
    }

    @Override
    public void clear() {
        tasks.clear();
    }

}
