package ru.baulina.tm.constant;

public class DataConstant {

    public static final String FILE_BINARY = "./data.bin";

    public static final String FILE_BASE64 = "./data.base64";

    public static final String FILE_XML = "./data.xml";

    public static final String FILE_JSON = "./data.json";

}
