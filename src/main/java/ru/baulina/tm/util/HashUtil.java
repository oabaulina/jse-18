package ru.baulina.tm.util;

import java.math.BigInteger;
import java.security.MessageDigest;

public interface HashUtil {

    String SECRET = "753951852456";

    Integer ITERATION = 30333;

    static String salt(final String value) {
        if (value == null) return null;
        String result = value;
        for (int i = 0; i < ITERATION; i++) {
            result = md5hashing(SECRET + value + SECRET);
        }
        return result;
    }

    static String md5hashing(final String text) {
        String hashtext = null;
        try
        {
            String plaintext = text;
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.reset();
            md.update(plaintext.getBytes());
            final byte[] digest = md.digest();
            BigInteger bigInt = new BigInteger(1,digest);
            hashtext = bigInt.toString(16);
            while(hashtext.length() < 32 ){
                hashtext = "0"+hashtext;
            }
        } catch (Exception e)
        {
           System.out.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return hashtext;
    }

}
