package ru.baulina.tm.util;

public interface SystemInformation {

    int availableProcessors = Runtime.getRuntime().availableProcessors();

    long freeMemory = Runtime.getRuntime().freeMemory();

    String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);

    long maxMemory = Runtime.getRuntime().maxMemory();

    String maxMemoryValue = NumberUtil.formatBytes(maxMemory);

    String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;

    long totalMemory = Runtime.getRuntime().totalMemory();

    String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);

    long usedMemory = totalMemory - freeMemory;

    String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);

}
