package ru.baulina.tm.exception.system;

public class IncorrectCommandException extends RuntimeException{

    public IncorrectCommandException() {
        super("Error! Command not exist...");
    }

}
