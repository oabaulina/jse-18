package ru.baulina.tm.exception.system;

public class EmptyCommandException extends RuntimeException{

    public EmptyCommandException() {
        super("Error! Command is empty...");
    }

}
