package ru.baulina.tm.entity;

import java.io.Serializable;
import java.util.Random;

public abstract class AbstractEntity implements Serializable {

    private long id = Math.abs(new Random().nextLong());

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}
