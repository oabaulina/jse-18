package ru.baulina.tm.service;

import ru.baulina.tm.api.service.IDomainService;
import ru.baulina.tm.api.service.IProjectService;
import ru.baulina.tm.api.service.ITaskService;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.dto.Domain;

public class DomainService implements IDomainService {

    private final IUserService userService;

    private final IProjectService projectService;

    private final ITaskService taskService;

    public DomainService(
            final IUserService userService,
            final IProjectService projectService,
            final ITaskService taskService
    ) {
        this.userService = userService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    public void load(Domain domain) {
        if (domain == null) return;
        projectService.load(domain.getProjects());
        taskService.load(domain.getTasks());
        userService.load(domain.getUsers());
    }

    @Override
    public void export(Domain domain) {
        if (domain == null) return;
        domain.setProjects(projectService.getListProject());
        domain.setTasks(taskService.getListTask());
        domain.setUsers(userService.getListUser());
    }

}
