package ru.baulina.tm.service;

import ru.baulina.tm.api.repository.ITaskRepository;
import ru.baulina.tm.api.service.ITaskService;
import ru.baulina.tm.exception.empty.EmptyIdException;
import ru.baulina.tm.exception.empty.EmptyPasswordException;
import ru.baulina.tm.exception.incorrect.IncorrectIndexException;
import ru.baulina.tm.exception.empty.EmptyNameException;
import ru.baulina.tm.entity.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final Long userId, final String name) {
        if (userId == null || userId < 0) throw new EmptyPasswordException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = new Task();
        task.setName(name);
        taskRepository.add(userId,task);
    }

    @Override
    public void create(final Long userId, final String name, final String description) {
        if (userId == null || userId < 0) throw new EmptyPasswordException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(userId, task);
    }

    @Override
    public void add(final Long userId, final Task task) {
        if (userId == null || userId < 0) throw new EmptyPasswordException();
        if (task == null) return;
        taskRepository.add(userId, task);
    }

    @Override
    public void remove(final Long userId, final Task task) {
        if (userId == null || userId < 0) throw new EmptyPasswordException();
        if (task == null) return;
        taskRepository.remove(userId, task);
    }

    @Override
    public List<Task> findAll(final Long userId) {
        if (userId == null || userId < 0) throw new EmptyPasswordException();
         return taskRepository.findAll(userId);
    }

    @Override
    public List<Task> getListTask() {
        return taskRepository.getListTask();
    }

    @Override
    public void clear(final Long userId) {
        if (userId == null || userId < 0) throw new EmptyPasswordException();
        taskRepository.clear(userId);
    }

    @Override
    public void load(List<Task> tasks) {
        taskRepository.load(tasks);
    }

    @Override
    public Task findOneById(final Long userId, Long id) {
        if (userId == null || userId < 0) throw new EmptyPasswordException();
        if (id == null || id < 0) throw new EmptyIdException();
        return taskRepository.findOneById(userId, id);
    }

    @Override
    public Task findOneByIndex(final Long userId, final Integer index) {
        if (userId == null || userId < 0) throw new EmptyPasswordException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return taskRepository.findOneByIndex(userId, index);
    }

    @Override
    public Task findOneByName(final Long userId, final String name) {
        if (userId == null || userId < 0) throw new EmptyPasswordException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findOneByName(userId, name);
    }

    @Override
    public Task removeOneById(final Long userId, final Long id) {
        if (userId == null || userId < 0) throw new EmptyPasswordException();
         if (id == null || id < 0) throw new EmptyIdException();
         return taskRepository.removeOneById(userId, id);
    }

    @Override
    public Task removeOneByIndex(final Long userId, final Integer index) {
        if (userId == null || userId < 0) throw new EmptyPasswordException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return taskRepository.removeOneByIndex(userId, index);
    }

    @Override
    public Task removeOneByName(final Long userId, final String name) {
        if (userId == null || userId < 0) throw new EmptyPasswordException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.removeOneByName(userId, name);
    }

    @Override
    public Task updateTaskById(final Long userId, final Long id, final String name, final String description) {
        if (userId == null || userId < 0) throw new EmptyPasswordException();
        if (id == null || id < 0) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskByIndex(final Long userId, final Integer index, final String name, final String description) {
        if (userId == null || userId < 0) throw new EmptyPasswordException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return null;
    }

}
