package ru.baulina.tm.api.service;

import ru.baulina.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandService {

    void add(AbstractCommand command);

    void remove(AbstractCommand command);

    List<AbstractCommand> findAll();

    public List<String> getCommandsNames();

    List<String> getArgs();

    void clear();

    AbstractCommand getByArg(String arg);

    AbstractCommand getByName(String name);

    AbstractCommand removeByName(String name);

    AbstractCommand removeByArg(String name);

}
