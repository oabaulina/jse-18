package ru.baulina.tm.api.service;

import ru.baulina.tm.entity.Task;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.enumerated.Role;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    void load(List<User> users);

    User findById(Long id);

    User findByLogin(String login);

    User removeUser(User user);

    User removeById(Long id);

    User removeByLogin(String login);

    User lockUserLogin(String login);

    User unlockUserLogin(String login);

    List<User> getListUser();

}
