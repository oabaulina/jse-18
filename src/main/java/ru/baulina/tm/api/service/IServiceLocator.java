package ru.baulina.tm.api.service;

public interface IServiceLocator {

    IUserService getUserService();

    IAuthService getAuthService();

    ICommandService getCommandService();

    ITaskService getTaskService();

    IProjectService getProjectService();

    IDomainService getDomainService();

}
