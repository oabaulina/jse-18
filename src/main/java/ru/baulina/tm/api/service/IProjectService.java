package ru.baulina.tm.api.service;

import ru.baulina.tm.entity.Project;

import java.util.List;

public interface IProjectService {

    void create(Long userId, String name);

    void create(Long userId, String name, String description);

    void add(Long userId, Project project);

    void remove(Long userId, Project project);

    List<Project> findAll(Long userId);

    List<Project> getListProject();

    void clear(Long userId);

    void load(List<Project> projects);

    Project findOneById(Long userId, Long id);

    Project findOneByIndex(Long userId, Integer index);

    Project findOneByName(Long userId, String name);

    Project removeOneById(Long userId, Long id);

    Project removeOneByIndex(Long userId, Integer index);

    Project removeOneByName(Long userId, String name);

    Project updateTaskById(Long userId, Long id, String name, String  description);

    Project updateTaskByIndex(Long userId, Integer index, String name, String  description);

}
