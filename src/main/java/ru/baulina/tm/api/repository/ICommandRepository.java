package ru.baulina.tm.api.repository;

import ru.baulina.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandRepository {

    void add(AbstractCommand command);

    void remove(AbstractCommand command);

    List<AbstractCommand> findAll();

    List<String> getCommandsNames();

    List<String> getArgs();

    void clear();

    AbstractCommand getByArg(String arg);

    AbstractCommand getByName(String name);

    AbstractCommand removeByName(String name);

    AbstractCommand removeByArg(String name);

}
