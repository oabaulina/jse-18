package ru.baulina.tm.api.repository;

import ru.baulina.tm.entity.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Long userId, Project project);

    void remove(Long userId, Project project);

    List<Project> findAll(Long userId);

    List<Project> getListProject();

    void clear(Long userId);

    Project findOneById(Long userId, Long id);

    Project findOneByIndex(Long userId, Integer index);

    Project findOneByName(Long userId, String name);

    Project removeOneById(Long userId, Long id);

    Project removeOneByIndex(Long userId, Integer index);

    Project removeOneByName(Long userId, String name);

    void merge(List<Project> projects);

    void merge(Project... projects);

    Project merge(Project project);

    void load(List<Project> projects);

    void load(Project... projects);

    void clear();

}
