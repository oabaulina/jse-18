package ru.baulina.tm.api.repository;

import ru.baulina.tm.entity.Task;
import ru.baulina.tm.entity.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User add(User user);

    User findById(Long id);

    User findByLogin(String login);

    User removeUser(User user);

    User removeById(Long id);

    User removeByLogin(String login);

    void merge(List<User> users);

    void merge(User... users);

    User merge(User task);

    void load(List<User> users);

    void load(User... users);

    void clear();

    List<User> getListUser();

}
