package ru.baulina.tm.command.data;

import ru.baulina.tm.api.service.IDomainService;
import ru.baulina.tm.constant.DataConstant;
import ru.baulina.tm.dto.Domain;
import ru.baulina.tm.exception.data.DataSaveException;

import java.io.*;
import java.nio.file.Files;
import java.util.Base64;

public final class DataBase64SaveCommand extends AbstractDataCommand {

    @Override
    public String name() {
        return "data-base64-save";
    }

    @Override
    public String description() {
        return "Save data to base64 file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA BASE64 SAVE]");
        final IDomainService domainService = serviceLocator.getDomainService();
        final Domain domain = new Domain();
        domainService.export(domain);
        final File file = new File(DataConstant.FILE_BASE64);
        try (
                final FileOutputStream fileOutputStream = new FileOutputStream(file);
                final ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
                final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteOutputStream)
        ) {
            Files.deleteIfExists(file.toPath());
            Files.createFile(file.toPath());
            objectOutputStream.writeObject(domain);
            final byte[] bytes = byteOutputStream.toByteArray();
            final String Base64Encoded = Base64.getEncoder().encodeToString(bytes);
            final byte[] bytesEncoded = Base64Encoded.getBytes();

            fileOutputStream.write(bytesEncoded);
            fileOutputStream.flush();
       } catch (IOException e) {
            throw new DataSaveException(e);
        }

        System.out.println("[OK]");
        System.out.println();
    }

}
