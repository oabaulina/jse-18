package ru.baulina.tm.command.data;

import ru.baulina.tm.constant.DataConstant;
import ru.baulina.tm.exception.data.DataDeleteException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public final class DataXmlClearCommand extends AbstractDataCommand {

    @Override
    public String name() {
        return "data-xml-clear";
    }

    @Override
    public String description() {
        return "Delete xml data file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA XML DELETE]");
        final File file = new File(DataConstant.FILE_XML);
        try {
            Files.deleteIfExists(file.toPath());
        } catch (IOException e) {
            throw new DataDeleteException();
        }
        System.out.println("[OK]");
        System.out.println();
    }

}
