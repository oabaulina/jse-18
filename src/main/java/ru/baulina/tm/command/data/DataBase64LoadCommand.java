package ru.baulina.tm.command.data;

import ru.baulina.tm.api.service.IProjectService;
import ru.baulina.tm.api.service.ITaskService;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.constant.DataConstant;
import ru.baulina.tm.dto.Domain;
import ru.baulina.tm.exception.data.DataLoadException;

import java.io.*;
import java.nio.file.Files;
import java.util.Base64;

public final class DataBase64LoadCommand extends AbstractDataCommand {

    @Override
    public String name() {
        return "data-base64-load";
    }

    @Override
    public String description() {
        return "Load data from base64 file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA BASE64 LOAD]");
        final File file = new File(DataConstant.FILE_BASE64);
        try {
            final byte[] bytesEncoded = Files.readAllBytes(file.toPath());
            final byte[] bytes = Base64.getDecoder().decode(bytesEncoded);
            final ByteArrayInputStream byteInputStream = new ByteArrayInputStream(bytes);
            final ObjectInputStream objectInputStream = new ObjectInputStream(byteInputStream);
            final Domain domain = (Domain) objectInputStream.readObject();
            final IProjectService projectService = serviceLocator.getProjectService();
            final ITaskService taskService = serviceLocator.getTaskService();
            final IUserService userService = serviceLocator.getUserService();
            projectService.load(domain.getProjects());
            taskService.load(domain.getTasks());
            userService.load(domain.getUsers());
            objectInputStream.close();
            byteInputStream.close();
        } catch (IOException | ClassNotFoundException e) {
            throw new DataLoadException(e);
        }
        System.out.println("[LOGOUT CURRENT USER]");
        serviceLocator.getAuthService().logout();
        System.out.println("[OK]");
        System.out.println();
    }

}
