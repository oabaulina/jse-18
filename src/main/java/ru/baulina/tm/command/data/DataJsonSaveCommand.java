package ru.baulina.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.baulina.tm.api.service.IDomainService;
import ru.baulina.tm.constant.DataConstant;
import ru.baulina.tm.dto.Domain;
import ru.baulina.tm.exception.data.DataSaveException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

public final class DataJsonSaveCommand extends AbstractDataCommand {

    @Override
    public String name() {
        return "data-json-save";
    }

    @Override
    public String description() {
        return "Save json to binary file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA JSON SAVE]");
        final IDomainService domainService = serviceLocator.getDomainService();
        final Domain domain = new Domain();
        domainService.export(domain);
        final File file = new File(DataConstant.FILE_JSON);
        try (
                final FileOutputStream fileOutputStream = new FileOutputStream(file);
        ) {
            final ObjectMapper objectMapper = new ObjectMapper();
            final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            fileOutputStream.write(json.getBytes());
            fileOutputStream.flush();
        } catch (IOException e) {
            throw new DataSaveException(e);
        }

        System.out.println("[OK]");
        System.out.println();
    }

}
