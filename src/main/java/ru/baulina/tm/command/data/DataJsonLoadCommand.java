package ru.baulina.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.baulina.tm.api.service.IProjectService;
import ru.baulina.tm.api.service.ITaskService;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.constant.DataConstant;
import ru.baulina.tm.dto.Domain;
import ru.baulina.tm.entity.Project;
import ru.baulina.tm.entity.Task;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.exception.data.DataLoadException;

import java.io.*;
import java.util.Arrays;

public final class DataJsonLoadCommand extends AbstractDataCommand {

    @Override
    public String name() {
        return "data-json-load";
    }

    @Override
    public String description() {
        return "Load json from binary file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA JSON LOAD]");
        final File file = new File(DataConstant.FILE_JSON);
        try (
                final FileInputStream fileInputStream = new FileInputStream(file)
        ) {
            final ObjectMapper objectMapper = new ObjectMapper();
            final Domain domain = objectMapper.readValue(fileInputStream, Domain.class);
            final IProjectService projectService = serviceLocator.getProjectService();
            final ITaskService taskService = serviceLocator.getTaskService();
            final IUserService userService = serviceLocator.getUserService();
            projectService.load(domain.getProjects());
            taskService.load(domain.getTasks());
            userService.load(domain.getUsers());
        } catch (IOException e) {
            throw new DataLoadException(e);
        }
        System.out.println("[LOGOUT CURRENT USER]");
        serviceLocator.getAuthService().logout();
        System.out.println("[OK]");
        System.out.println();
    }

}
