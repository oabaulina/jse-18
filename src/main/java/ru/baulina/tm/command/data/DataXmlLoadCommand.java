package ru.baulina.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.baulina.tm.api.service.IProjectService;
import ru.baulina.tm.api.service.ITaskService;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.constant.DataConstant;
import ru.baulina.tm.dto.Domain;
import ru.baulina.tm.exception.data.DataLoadException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public final class DataXmlLoadCommand extends AbstractDataCommand {

    @Override
    public String name() {
        return "data-xml-load";
    }

    @Override
    public String description() {
        return "Load data from xml file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA XML LOAD]");
        final File file = new File(DataConstant.FILE_XML);
        try (
                final FileInputStream fileInputStream = new FileInputStream(file)
        ) {
            final ObjectMapper objectMapper = new XmlMapper();
            final Domain domain = objectMapper.readValue(fileInputStream, Domain.class);
            final IProjectService projectService = serviceLocator.getProjectService();
            final ITaskService taskService = serviceLocator.getTaskService();
            final IUserService userService = serviceLocator.getUserService();
            taskService.load(domain.getTasks());
            userService.load(domain.getUsers());
            projectService.load(domain.getProjects());
        } catch (IOException e) {
            throw new DataLoadException(e);
        }
        System.out.println("[LOGOUT CURRENT USER]");
        serviceLocator.getAuthService().logout();
        System.out.println("[OK]");
        System.out.println();
    }

}
