package ru.baulina.tm.command.data;

import ru.baulina.tm.constant.DataConstant;
import ru.baulina.tm.exception.data.DataDeleteException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public final class DataBinaryClearCommand extends AbstractDataCommand {

    @Override
    public String name() {
        return "data-bin-clear";
    }

    @Override
    public String description() {
        return "Delete bin data file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA BINARY DELETE]");
        final File file = new File(DataConstant.FILE_BINARY);
        try {
            Files.deleteIfExists(file.toPath());
        } catch (IOException e) {
            throw new DataDeleteException();
        }
        System.out.println("[OK]");
        System.out.println();
    }

}
