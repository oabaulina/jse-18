package ru.baulina.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.baulina.tm.api.service.IDomainService;
import ru.baulina.tm.constant.DataConstant;
import ru.baulina.tm.dto.Domain;
import ru.baulina.tm.exception.data.DataSaveException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public final class DataXmlSaveCommand extends AbstractDataCommand {

    @Override
    public String name() {
        return "data-xml-save";
    }

    @Override
    public String description() {
        return "Save data to xml file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA XML SAVE]");
        final IDomainService domainService = serviceLocator.getDomainService();
        final Domain domain = new Domain();
        domainService.export(domain);
        final File file = new File(DataConstant.FILE_XML);
        try (
                final FileOutputStream fileOutputStream = new FileOutputStream(file);
        ) {
            final ObjectMapper objectMapper = new XmlMapper();
            final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            fileOutputStream.write(xml.getBytes());
            fileOutputStream.flush();
        } catch (IOException e) {
            throw new DataSaveException(e);
        }
        System.out.println("[OK]");
        System.out.println();
    }

}
