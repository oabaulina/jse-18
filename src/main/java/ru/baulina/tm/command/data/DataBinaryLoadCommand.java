package ru.baulina.tm.command.data;

import ru.baulina.tm.api.service.IProjectService;
import ru.baulina.tm.api.service.ITaskService;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.constant.DataConstant;
import ru.baulina.tm.dto.Domain;
import ru.baulina.tm.exception.data.DataLoadException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public final class DataBinaryLoadCommand extends AbstractDataCommand {

    @Override
    public String name() {
        return "data-bin-load";
    }

    @Override
    public String description() {
        return "Load data from binary file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA BINARY LOAD]");
        final File file = new File(DataConstant.FILE_BINARY);
        try (
            final FileInputStream fileInputStream = new FileInputStream(file);
            final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)
        ) {
            final Domain domain = (Domain) objectInputStream.readObject();
            final IProjectService projectService = serviceLocator.getProjectService();
            final ITaskService taskService = serviceLocator.getTaskService();
            final IUserService userService = serviceLocator.getUserService();
            projectService.load(domain.getProjects());
            taskService.load(domain.getTasks());
            userService.load(domain.getUsers());
        } catch (IOException | ClassNotFoundException e) {
            throw new DataLoadException(e);
        }
        System.out.println("[LOGOUT CURRENT USER]");
        serviceLocator.getAuthService().logout();
        System.out.println("[OK]");
        System.out.println();
    }

}
