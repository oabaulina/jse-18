package ru.baulina.tm.command.data;

import ru.baulina.tm.constant.DataConstant;
import ru.baulina.tm.exception.data.DataDeleteException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public final class DataJsonClearCommand extends AbstractDataCommand {

    @Override
    public String name() {
        return "data-json-clear";
    }

    @Override
    public String description() {
        return "Delete json data file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA JSON DELETE]");
        final File file = new File(DataConstant.FILE_JSON);
        try {
            Files.deleteIfExists(file.toPath());
        } catch (IOException e) {
            throw new DataDeleteException();
        }
        System.out.println("[OK]");
        System.out.println();
    }

}
