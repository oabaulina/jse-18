package ru.baulina.tm.command.data;

import ru.baulina.tm.constant.DataConstant;
import ru.baulina.tm.exception.data.DataDeleteException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public final class DataBase64ClearCommand extends AbstractDataCommand {

    @Override
    public String name() {
        return "data-base64-clear";
    }

    @Override
    public String description() {
        return "Delete base64 data file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA BASE64 DELETE]");
        final File file = new File(DataConstant.FILE_BASE64);
        try {
            Files.deleteIfExists(file.toPath());
        } catch (IOException e) {
            throw new DataDeleteException();
        }
        System.out.println("[OK]");
        System.out.println();
    }

}
