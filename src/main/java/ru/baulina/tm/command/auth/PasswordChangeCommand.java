package ru.baulina.tm.command.auth;

import ru.baulina.tm.util.TerminalUtil;

public final class PasswordChangeCommand extends AbstractAuthCommand {

    final String LOGOUT_NAME = "change-password";

    final String LOGOUT_DESCRIPTION = "Change user's password.";

    @Override
    public String name() {
        return LOGOUT_NAME;
    }

    @Override
    public String description() {
        return LOGOUT_DESCRIPTION;
    }

    @Override
    public void execute() {
        serviceLocator.getAuthService().isAuth();
        System.out.println("[CHANGE_PASSWORD]");
        System.out.println("ENTER OLD PASSWORD:");
        final String passwordOld = TerminalUtil.nextLine();
        System.out.println("ENTER NEW PASSWORD:");
        final String passwordNew = TerminalUtil.nextLine();
        serviceLocator.getAuthService().changePassword(passwordOld, passwordNew);
        System.out.println("[OK]");
        System.out.println("");
    }

}
