package ru.baulina.tm.command.auth;

import ru.baulina.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractAuthCommand{

    final String USER_REMOVE_NAME = "remove-by-login";

    final String USER_REMOVE_DESCRIPTION = "Remove user by login.";

    @Override
    public String name() {
        return USER_REMOVE_NAME;
    }

    @Override
    public String description() {
        return USER_REMOVE_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE_USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().removeByLogin(login);
        System.out.println("[OK]");
        System.out.println("");
    }
}
