package ru.baulina.tm.command.auth;

import ru.baulina.tm.util.TerminalUtil;

public final class ProfileOfUserChangeCommand extends AbstractAuthCommand {

    final String PROFILE_OF_USER_CHANGE_NAME = "change-profiler-of-user";

    final String PROFILE_OF_USER_CHANGE_DESCRIPTION = "Change user's profiler.";

    @Override
    public String name() {
        return PROFILE_OF_USER_CHANGE_NAME;
    }

    @Override
    public String description() {
        return PROFILE_OF_USER_CHANGE_DESCRIPTION;
    }

    @Override
    public void execute() {
        serviceLocator.getAuthService().isAuth();
        System.out.println("[CHANGE_PROFILE_OF_USER]");
        System.out.println("ENTER E-MAIL: ");
        final String newEmail = TerminalUtil.nextLine();
        System.out.println("ENTER FEST NAME: ");
        final String newFestName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME: ");
        final String newLastName = TerminalUtil.nextLine();
        serviceLocator.getAuthService().changeUser(newEmail, newFestName, newLastName);
        System.out.println("[OK]");
        System.out.println("");
    }

}
