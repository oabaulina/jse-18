package ru.baulina.tm.command.auth;

import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.util.TerminalUtil;

public class LoginCommand extends AbstractCommand {

    final String LOGIN_NAME = "login";

    final String LOGIN_DESCRIPTION = "Sign in.";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return LOGIN_NAME;
    }

    @Override
    public String description() {
        return LOGIN_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
        System.out.println("[OK]");
    }

}
