package ru.baulina.tm.command.project;

import ru.baulina.tm.entity.Project;
import ru.baulina.tm.util.TerminalUtil;

public final class ProjectShowByNameCommand extends AbstractProjectCommand {

    final String PROJECT_SHOW_BY_NAME_NAME = "project-show-by-name";

    final String PROJECT_SHOW_BY_NAME_DESCRIPTION = "Show project by name.";

    @Override
    public String name() {
        return PROJECT_SHOW_BY_NAME_NAME;
    }

    @Override
    public String description() {
        return PROJECT_SHOW_BY_NAME_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME");
        final String name = TerminalUtil.nextLine();
        final Long userId = serviceLocator.getAuthService().getUserId();
        final Project project = serviceLocator.getProjectService().findOneByName(userId, name);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
        System.out.println("");
    }

}
