package ru.baulina.tm.command.project;

import ru.baulina.tm.entity.Project;
import ru.baulina.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    final String PROJECT_SHOW_BY_ID_NAME = "project-show-by-id";

    final String PROJECT_SHOW_BY_ID_DESCRIPTION = "Show project by id.";

    @Override
    public String name() {
        return PROJECT_SHOW_BY_ID_NAME;
    }

    @Override
    public String description() {
        return PROJECT_SHOW_BY_ID_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID");
        final Long id = TerminalUtil.nexLong();
        final Long userId = serviceLocator.getAuthService().getUserId();
        final Project project = serviceLocator.getProjectService().findOneById(userId, id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
        System.out.println("");
    }

}
