package ru.baulina.tm.command.project;

import ru.baulina.tm.entity.Project;
import ru.baulina.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    final String PROJECT_REMOVE_BY_ID_NAME = "project-remove-by-id";

    final String PROJECT_REMOVE_BY_ID_DESCRIPTION = "Remove project by id.";

    @Override
    public String name() {
        return PROJECT_REMOVE_BY_ID_NAME;
    }

    @Override
    public String description() {
        return PROJECT_REMOVE_BY_ID_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER ID");
        final Long id = TerminalUtil.nexLong();
        final Long userId = serviceLocator.getAuthService().getUserId();
        final Project project = serviceLocator.getProjectService().removeOneById(userId, id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        System.out.println("");
    }

}
