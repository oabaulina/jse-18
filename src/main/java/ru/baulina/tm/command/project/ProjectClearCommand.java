package ru.baulina.tm.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand {

    final String PROJECT_CLEAR_NAME = "project-clear";

    final String PROJECT_CLEAR_DESCRIPTION = "Remove all projects.";

    @Override
    public String name() {
        return PROJECT_CLEAR_NAME;
    }

    @Override
    public String description() {
        return PROJECT_CLEAR_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECT]");
        final Long userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getProjectService().clear(userId);
        System.out.println("[OK]");
        System.out.println();
    }

}
