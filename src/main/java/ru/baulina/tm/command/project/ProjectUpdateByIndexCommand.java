package ru.baulina.tm.command.project;

import ru.baulina.tm.entity.Project;
import ru.baulina.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    final String PROJECT_UPDATE_BY_INDEX_NAME = "project-update_by_index";

    final String PROJECT_UPDATE_BY_INDEX_DESCRIPTION = "Update project by index.";

    @Override
    public String name() {
        return PROJECT_UPDATE_BY_INDEX_NAME;
    }

    @Override
    public String description() {
        return PROJECT_UPDATE_BY_INDEX_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index  = TerminalUtil.nexInt() -1;
        final Long userId = serviceLocator.getAuthService().getUserId();
        final Project project = serviceLocator.getProjectService().findOneByIndex(userId, index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name  = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description  = TerminalUtil.nextLine();
        final Project projectUpdated = serviceLocator.getProjectService().updateTaskByIndex(userId, index, name, description);
        if (projectUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
        System.out.println("");
    }

}
