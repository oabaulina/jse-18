package ru.baulina.tm.command.project;

import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.enumerated.Role;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.USER, Role.ADMIN};
    }

}
