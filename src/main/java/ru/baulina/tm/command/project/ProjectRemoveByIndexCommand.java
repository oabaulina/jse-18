package ru.baulina.tm.command.project;

import ru.baulina.tm.entity.Project;
import ru.baulina.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    final String PROJECT_REMOVE_BY_INDEX_NAME = "project-remove-by-index";

    final String PROJECT_REMOVE_BY_INDEX_DESCRIPTION = "Remove project by index.";

    @Override
    public String name() {
        return PROJECT_REMOVE_BY_INDEX_NAME;
    }

    @Override
    public String description() {
        return PROJECT_REMOVE_BY_INDEX_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER INDEX");
        final Integer index = TerminalUtil.nexInt() -1;
        final Long userId = serviceLocator.getAuthService().getUserId();
        final Project project = serviceLocator.getProjectService().removeOneByIndex(userId, index);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        System.out.println("");
    }

}
