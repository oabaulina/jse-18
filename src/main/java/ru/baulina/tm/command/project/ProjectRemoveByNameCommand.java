package ru.baulina.tm.command.project;

import ru.baulina.tm.entity.Project;
import ru.baulina.tm.util.TerminalUtil;

public final class ProjectRemoveByNameCommand extends AbstractProjectCommand {

    final String PROJECT_REMOVE_BY_NAME_NAME = "project-remove-by-name";

    final String PROJECT_REMOVE_BY_NAME_DESCRIPTION = "Remove project by name.";

    @Override
    public String name() {
        return PROJECT_REMOVE_BY_NAME_NAME;
    }

    @Override
    public String description() {
        return PROJECT_REMOVE_BY_NAME_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER NAME");
        final String name = TerminalUtil.nextLine();
        final Long userId = serviceLocator.getAuthService().getUserId();
        final Project project = serviceLocator.getProjectService().removeOneByName(userId, name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        System.out.println("");
    }

}
