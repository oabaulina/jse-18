package ru.baulina.tm.command.system;

import ru.baulina.tm.command.AbstractCommand;

import java.util.List;

public final class HelpCommand extends AbstractCommand {

    final String HELP_ARG = "-h";

    final String HELP_NAME = "help";

    final String HELP_DESCRIPTION = "Display list of terminal commands.";

    @Override
    public String arg() {
        return HELP_ARG;
    }

    @Override
    public String name() {
        return HELP_NAME;
    }

    @Override
    public String description() {
        return HELP_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final List<AbstractCommand> commands = serviceLocator.getCommandService().findAll();
        for (final AbstractCommand command: commands) System.out.println(command);
        System.out.println("[OK]");
        System.out.println();
    }

}
