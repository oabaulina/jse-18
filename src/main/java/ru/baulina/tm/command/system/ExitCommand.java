package ru.baulina.tm.command.system;

import ru.baulina.tm.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {

    final String EXIT_NAME = "exit";

    final String EXIT_DESCRIPTION = "Close application.";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return EXIT_NAME;
    }

    @Override
    public String description() {
        return EXIT_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
