package ru.baulina.tm.command.system;

import ru.baulina.tm.command.AbstractCommand;

import java.util.Arrays;
import java.util.List;

public final class CommandShowCommand extends AbstractCommand {

    final String COMMANDS_ARG = "-cmd";

    final String COMMANDS_NAME = "commands";

    final String COMMANDS_DESCRIPTION = "Show program commands.";

    @Override
    public String arg() {
        return COMMANDS_ARG;
    }

    @Override
    public String name() {
        return COMMANDS_NAME;
    }

    @Override
    public String description() {
        return COMMANDS_DESCRIPTION;
    }

    @Override
    public void execute() {
        final List<String> commands = serviceLocator.getCommandService().getCommandsNames();
        System.out.println(Arrays.toString(commands.toArray()));
        System.out.println();
    }

}
