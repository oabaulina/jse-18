package ru.baulina.tm.command.system;

import ru.baulina.tm.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {

    final String ABOUT_ARG = "-a";

    final String ABOUT_NAME = "about";

    final String ABOUT_DESCRIPTION = "Display developer info.";

    @Override
    public String arg() {
        return ABOUT_ARG;
    }

    @Override
    public String name() {
        return ABOUT_NAME;
    }

    @Override
    public String description() {
        return ABOUT_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Olga Baulina");
        System.out.println("golovolomkacom@gmail.com");
        System.out.println("OK");
        System.out.println();
    }

}
