package ru.baulina.tm.command.system;

import ru.baulina.tm.command.AbstractCommand;

import java.util.Arrays;
import java.util.List;

public final class ArgumentShowCommand extends AbstractCommand {

    final String ARGUMENTS_ARG = "-arg";

    final String ARGUMENTS_NAME = "arguments";

    final String ARGUMENTS_DESCRIPTION = "Show program arguments.";

    @Override
    public String arg() {
        return ARGUMENTS_ARG;
    }

    @Override
    public String name() {
        return ARGUMENTS_NAME;
    }

    @Override
    public String description() {
        return ARGUMENTS_DESCRIPTION;
    }

    @Override
    public void execute() {
        final List<String> arguments = serviceLocator.getCommandService().getArgs();
        System.out.println(Arrays.toString(arguments.toArray()));
        System.out.println();
    }

}
