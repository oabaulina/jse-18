package ru.baulina.tm.command.task;

import ru.baulina.tm.entity.Task;
import ru.baulina.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    final String TASK_SHOW_BY_ID_NAME = "task-show-by-id";

    final String TASK_SHOW_BY_ID_DESCRIPTION = "Show task by id.";

    @Override
    public String name() {
        return TASK_SHOW_BY_ID_NAME;
    }

    @Override
    public String description() {
        return TASK_SHOW_BY_ID_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID");
        final Long id = TerminalUtil.nexLong();
        final Long userId = serviceLocator.getAuthService().getUserId();
        final Task task = serviceLocator.getTaskService().findOneById(userId, id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
        System.out.println("");
    }

}
