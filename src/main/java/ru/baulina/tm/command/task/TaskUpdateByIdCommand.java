package ru.baulina.tm.command.task;

import ru.baulina.tm.entity.Task;
import ru.baulina.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    final String TASK_UPDATE_BY_ID_NAME = "task-update-by-id";

    final String TASK_UPDATE_BY_ID_DESCRIPTION = "Update task by id.";

    @Override
    public String name() {
        return TASK_UPDATE_BY_ID_NAME;
    }

    @Override
    public String description() {
        return TASK_UPDATE_BY_ID_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER ID:");
        final Long id  = TerminalUtil.nexLong();
        final Long userId = serviceLocator.getAuthService().getUserId();
        final Task task = serviceLocator.getTaskService().findOneById(userId, id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name  = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description  = TerminalUtil.nextLine();
        final Task taskUpdated = serviceLocator.getTaskService().updateTaskById(userId, id, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
        System.out.println("");
    }

}
