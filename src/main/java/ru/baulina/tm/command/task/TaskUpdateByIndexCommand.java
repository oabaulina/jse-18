package ru.baulina.tm.command.task;

import ru.baulina.tm.entity.Task;
import ru.baulina.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    final String TASK_UPDATE_BY_INDEX_NAME = "task-update_by_index";

    final String TASK_UPDATE_BY_INDEX_DESCRIPTION = "Update task by index.";

    @Override
    public String name() {
        return TASK_UPDATE_BY_INDEX_NAME;
    }

    @Override
    public String description() {
        return TASK_UPDATE_BY_INDEX_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index  = TerminalUtil.nexInt() -1;
        final Long userId = serviceLocator.getAuthService().getUserId();
        final Task task = serviceLocator.getTaskService().findOneByIndex(userId, index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name  = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description  = TerminalUtil.nextLine();
        final Task taskUpdated = serviceLocator.getTaskService().updateTaskByIndex(userId, index, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
        System.out.println("");
    }

}
